(ns hwo2014bot.race
  (:require [hwo2014bot.channel :as channel]
            [hwo2014bot.lane-strategy :as lane-strategy]))

(def state (atom {}))

(defn my-car []
  (get @state :car))

(defn track-pieces []
  (get-in @state [:game :race :track :pieces]))

(defn laps []
  (or (get-in @state [:game :race :raceSession :laps]) 1))

(defn positions []
  (get @state :positions))

(defn current-position []
  (get (positions) (my-car)))

(defn lap []
  (get-in (current-position) [:piecePosition :lap]))

(defn piece-index
  ([] (piece-index (my-car)))
  ([car]
     (get-in (positions) [car :piecePosition :pieceIndex])))

(defn in-piece-distance []
  (or (get-in (current-position) [:piecePosition :inPieceDistance]) 0))

(defn start-lane-index []
  (get-in (current-position) [:piecePosition :lane :startLaneIndex]))

(defn end-lane-index []
  (get-in (current-position) [:piecePosition :lane :endLaneIndex]))

(defn race-piece-index []
  (+ (* (lap) (count (track-pieces))) (piece-index)))

(defn race-pieces []
  (apply concat (repeat (laps) (track-pieces))))

(defn lane-radius [lane-index]
  (->> (get-in @state [:game :race :track :lanes])
       (filter #(= lane-index (:index %)))
       first
       :distanceFromCenter))

(defn current-throttle []
  (get @state :throttle))

(defn next-lane-switch []
  (some (fn [[{:keys [min-index max-index] :as lane-switch} laps]]
          (when (and (<= min-index (piece-index))
                     (< (piece-index) max-index)
                     (not (laps (lap))))
            lane-switch))
          (:lane-switches @state)))

(defn current-speed []
  (or (get-in @state [:speeds (my-car)]) 0))

(defn turbo-available? []
  (get @state :turbo-available))

(defn turbo-start-index []
  (get @state :turbo-start-index))

(defn last-lap-turbo-index []
  (get @state :last-lap-turbo-index))

(defn last-lap? []
  (= (lap) (dec (laps))))

(defn current-tick []
  (get @state :tick))

(defn with-tick [message]
  (assoc message :gameTick (current-tick)))

(defn use-turbo []
  (when (turbo-available?)
    (channel/send (with-tick {:msgType "turbo" :data "So long, suckers!"})))
  (swap! state assoc :turbo-available false))

(defn switch-lane []
  (when-let [{:keys [switch] :as lane-switch} (next-lane-switch)]
    (channel/send (with-tick {:msgType "switchLane"
                              :data ({:left "Left" :right "Right"} switch)}))
    (swap! state update-in [:lane-switches lane-switch] conj (lap))))

(defn ping []
  (channel/send (with-tick {:msgType "ping"})))

(defn throttle [v]
  (channel/send (with-tick {:msgType "throttle" :data v}))
  (swap! state assoc :throttle v))

(defn join [botname botkey]
  (channel/send {:msgType "join" :data {:name botname :key botkey}}))

(defn piece-length [my-lane {:keys [length radius angle]}]
  (if length
    length
    (* (/ (Math/abs angle) 360)
       2
       Math/PI
       (Math/abs (- (* (Math/signum angle) radius) (lane-radius my-lane))))))

(defn my-lane []
  (get-in (current-position) [:piecePosition :lane :endLaneIndex]))

(defn take-for-length [length pieces]
  (loop [length length remaining pieces result []]
    (if (and length (pos? length))
      (recur (- length (piece-length (my-lane) (first remaining)))
             (rest remaining)
             (conj result (first remaining)))
      result)))

(defn next-pieces [factor]
  (->> (track-pieces)
       (map-indexed #(assoc %2 :index %1))
       (cycle)
       (drop (inc (piece-index)))
       (take-for-length (* factor (+ (current-speed) (in-piece-distance))))))

(defn next-piece-indexes [factor]
  (map :index (next-pieces factor)))

(defn calculate-speed [old-position new-position]
  (when (and old-position new-position)
    (let [old-piece-index (get-in old-position [:piecePosition :pieceIndex])
          new-piece-index (get-in new-position [:piecePosition :pieceIndex])
          piece-count (inc (- (if (< new-piece-index old-piece-index)
                                (+ new-piece-index (count (track-pieces)))
                                new-piece-index)
                              old-piece-index))
          pieces (->> (cycle (track-pieces))
                      (drop old-piece-index)
                      (take piece-count))]
      (+ (apply + (map (partial piece-length (get-in new-position [:piecePosition :lane :startLaneIndex])) (butlast pieces)))
         (get-in new-position [:piecePosition :inPieceDistance])
         (- (get-in old-position [:piecePosition :inPieceDistance]))))))

(defn crashed?
  ([] (crashed? (my-car)))
  ([car]
   ((get @state :crashed-cars) car)))

(defn max-throttle []
  (get @state :max-throttle))

(defn stage []
  (get @state :stage))

(defn on-straight-piece? []
  (and (:length (nth (track-pieces) (piece-index)))
       (= (start-lane-index) (end-lane-index))))

(defn other-car-in-front? []
  (some
    #(and (= (piece-index) (get-in % [:piecePosition :pieceIndex]))
          (= (start-lane-index) (get-in % [:piecePosition :lane :startLaneIndex]))
          (= (end-lane-index) (get-in % [:piecePosition :lane :endLaneIndex]))
          (< (in-piece-distance) (get-in % [:piecePosition :inPieceDistance])))
    (keep (fn [[car position]] (when-not (or (crashed? car) (= car (my-car)))
                                 position))
          (positions))))

(defmulti handle-message :msgType)

(defmethod handle-message "yourCar" [{:keys [data]}]
  (swap! state assoc :car data)
  nil)

(defn- find-last-lap-turbo-index [pieces]
  (->> pieces
       (map-indexed #(assoc %2 :index %1))
       reverse
       (take-while :length)
       reverse
       first
       :index))

(def stage-transitions {:not-started :qualifying
                        :qualifying  :race})

(defn speed-observations []
  (get @state :speed-observations))

(defn init-speed-observations [pieces]
  (let [old (speed-observations)]
    (if (empty? old)
      (apply merge (map-indexed (fn [i {:keys [length]}]
                                  {i {:max-success (if length 4.0 2.0)}})
                                pieces))
      old)))

(defmethod handle-message "gameInit" [{:keys [data]}]
  (swap! state assoc
                 :crashed-cars #{}
                 :crashed-last-lap false
                 :turbo-available false
                 :last-lap-turbo-index (find-last-lap-turbo-index (get-in data [:race :track :pieces]))
                 :throttle nil
                 :positions nil
                 :game data
                 :stage (stage-transitions (stage))
                 :lane-switches (lane-strategy/calculate-switches (get-in data [:race :track :pieces]))
                 :speed-observations (init-speed-observations (get-in data [:race :track :pieces])))
  nil)

(defn with-nils [f & vals]
  (when-let [vals (seq (keep identity vals))]
    (apply f vals)))

(defn min-val [& vals]
  (apply min (keep identity vals)))

(defn max-val [& vals]
  (apply max (keep identity vals)))



(defmethod handle-message "gameStart" [{game-tick :gameTick}]
  game-tick)

(defn calculate-speeds [old-positions new-positions]
  (zipmap (map :id new-positions)
          (map #(calculate-speed (get old-positions (:id %)) %) new-positions)))

(defmethod handle-message "carPositions" [{data :data game-tick :gameTick}]
  (doseq [[id speed] (:speeds @state)]
    (swap! state update-in [:speed-observations (piece-index id) :max-success] #(with-nils max % speed)))
  (swap! state assoc :positions (zipmap (map :id data) data) :speeds (calculate-speeds (positions) data))
  game-tick)

(defmethod handle-message "crash" [{:keys [data]}]
  (swap! state (fn [state]
                 (-> state
                     (update-in [:speed-observations (piece-index data) :min-failure] #(with-nils min % (get-in state [:speeds data])))
                     (update-in [:crashed-cars] conj data))))
  (when (= (my-car) data)
    (swap! state assoc :throttle nil :crashed-last-lap true)
    (swap! state update-in [:max-throttle] #(max (- % 0.05) 0.1)))
  nil)

(defmethod handle-message "spawn" [{:keys [data]}]
  (swap! state update-in [:crashed-cars] disj data)
  nil)

(defn calculate-delta [crashed-last-lap stage lap]
  (if (or crashed-last-lap (>= lap 4))
    0.0
    (if (= :race stage) 0.05 0.1)))

(defmethod handle-message "lapFinished" [{:keys [data]}]
  (when (= (my-car) (:car data))
    (swap! state (fn [state]
                   (-> state
                       (update-in [:max-throttle] #(-> (+ % (calculate-delta (:crashed-last-lap state) (:stage state) (get-in data [:lapTime :lap])))
                                                       (min 1.0)))
                       (assoc :crashed-last-lap false)))))
  nil)

(defmethod handle-message "turboAvailable" [_]
  (swap! state assoc :turbo-available true)
  nil)

(defmethod handle-message :default [_]
  nil)

(defn last-message? [{message-type :msgType}]
  (= "tournamentEnd" message-type))

(defn start [{:keys [host port botname botkey]} tick-handler]
  (reset! state {:speed-observations {} :max-throttle 0.6 :stage :not-started})
  (channel/with-channel host port
    (join botname botkey)
    (loop []
      (let [message (channel/read)
            new-tick (handle-message message)]
        (when (and new-tick (not= (:tick @state) new-tick))
          (swap! state assoc :tick new-tick)
          (try
            (tick-handler)
            (println "Stage:" (stage) "Tick:" (current-tick) "Throttle:" (current-throttle) "Speed:" (current-speed))
            (catch Throwable t
              (println "Error in tick handler! Last position:" (current-position))
              (.printStackTrace t)
              (throttle 0.5))))
        (when-not (last-message? message)
          (recur))))))
