(ns hwo2014bot.core
  (:require [hwo2014bot.race :as race])
  (:gen-class))

(defn min-failure-speed [speed-observations]
  (when-let [speeds (seq (keep :min-failure speed-observations))]
    (apply min speeds)))

(defn max-success-speed [speed-observations]
  (when-let [speeds (seq (keep :max-success speed-observations))]
    (apply min speeds)))


(defn calculate-target-speed []
  (let [speed-observations (keep (race/speed-observations) (race/next-piece-indexes 5))
        min-failure (min-failure-speed speed-observations)
        max-success (max-success-speed speed-observations)]
    (cond
     min-failure (- min-failure 1.0)
     max-success max-success)))


(defn calculate-throttle []
  (let [target-speed (calculate-target-speed)]
    (if (and target-speed (> (race/current-speed) (max target-speed 1.0)))
      0.0
      (race/max-throttle))))

(defn bump-available? []
  (and (race/on-straight-piece?)
       (race/other-car-in-front?)))

(defn bump []
  (if (race/turbo-available?)
    (race/use-turbo)
    (race/throttle 1.0)))

(defn use-turbo? []
  (and (race/turbo-available?)
       (race/last-lap?)
       (= (race/piece-index) (race/last-lap-turbo-index))))

(defn tick-handler []
  (let [throttle (calculate-throttle)]
    (cond
     (race/crashed?)  (race/ping)
     (bump-available?) (bump)
     (use-turbo?) (race/use-turbo)
     (not= throttle (race/current-throttle))  (race/throttle throttle)    
     (race/next-lane-switch)  (race/switch-lane)
     :else (race/ping))))

(defn run
  ([] (run {}))
  ([opts]
     (race/start (merge {:host "testserver.helloworldopen.com" :port 8091 :botname "Turbojure" :botkey "9MNJRUHuk7cOKw"} opts) tick-handler)))

(defn -main [& [host port botname botkey]]
  (run {:host host :port (Integer/parseInt port) :botname botname :botkey botkey})
  (System/exit 0))
