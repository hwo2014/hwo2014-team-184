(ns hwo2014bot.channel
  (:refer-clojure :exclude [send read])
  (:require [aleph.tcp :as aleph]
            [lamina.core :as lamina]
            [gloss.core :as gloss]
            [clojure.data.json :as json]))

(def ^:dynamic *channel*)

(defn send [msg]
  #_(println (System/currentTimeMillis) ">" msg)
  (lamina/enqueue *channel* (json/write-str msg)))

(defn read []
  (let [msg (lamina/wait-for-message *channel*)]
    #_(println (System/currentTimeMillis) "<" msg)
    (json/read-str msg :key-fn keyword)))

(defn connect [host port]
  (lamina/wait-for-result
   (aleph/tcp-client {:host host
                      :port port
                      :frame (gloss/string :utf-8 :delimiters ["\n"])})))

(defn with-channel* [host port body-fn]
  (binding [*channel* (connect host port)]
    (body-fn)))

(defmacro with-channel [host port & body]
  `(with-channel* ~host ~port (fn [] ~@body)))
