(ns hwo2014bot.lane-strategy)

(defn race-pieces [pieces laps]
  (map-indexed #(assoc %2 :index %)
               (apply concat (repeat laps pieces))))

(defn abs [x]
  (if (neg? x) (- x) x))

(defn calculate-bend-length [angle-pieces angle-fn]
  (apply + (->> angle-pieces
                (filter (comp angle-fn :angle))
                (map (fn [{:keys [radius angle]}] (* radius (abs angle)))))))

(defn calculate-switch-dir [pieces]
  (let [angle-pieces (filter :angle (take-while (complement :switch) pieces))
        lefts (calculate-bend-length angle-pieces neg?)
        rights (calculate-bend-length angle-pieces pos?)]
    (if (> lefts rights)
      :left
      :right)))

(defn next-switch [min-index pieces]
  (let [{:keys [index switch]} (first pieces)]
    (when switch
      {:min-index min-index
       :max-index index
       :switch (calculate-switch-dir (rest pieces))})))

(defn calculate-switches [pieces]
  (let [switch-count (count (filter :switch pieces))]
    (loop [remaining (cycle (map-indexed #(assoc %2 :index %) pieces)) min-index 0 switches []]
      (if (< (count switches) switch-count)
        (if-let [switch (next-switch min-index remaining)]
          (recur (rest remaining) (inc (:max-index switch)) (conj switches switch))
          (recur (rest remaining) min-index switches))
        (zipmap switches (repeat #{}))))))
